<?php
// "HTTP/1.0 404 Not Found"
function status_header($code = 200) {
  $messages = [
    200 => "okej",
    300 => "Multiple Choices",
    301 => "Moved Permanently",
    302 => "Moved Temporarily",
    304 => "Not Modified",
    307 => "Temporary Redirect",
    400 => "Bad Request",
    401 => "Unauthorized",
  ];
  header("HTTP/1.0 ".$code." ".$messages[$code]);
}
status_header();



/*headers([
  "connection  keep-alive"
]);*/

// [header => värde]

function headers(array $headers = []) {
  foreach ($headers as $header => $value) {
      header("$header: $value");
  }
}
headers([
  "test" => "test",
  ]);

function redirect($url, $code = 302) {
    header("Location: " .$url, true, $code);
    die();
}

/*redirect("https://pixlr.com/");*/

/*header("Location: https://pixlr.com/");
exit;*/